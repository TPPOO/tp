package Source;
import java.io.FileNotFoundException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class LeituraArquivos
{
	
	void leituraCentrais() throws FileNotFoundException
	{
		final String FILENAME = "C:\\Users\\matth\\Desktop\\POO TP FINAL\\tp.git\\Trabalho Final\\dados\\centrais.dat";
		
		BufferedReader buff = null;
		FileReader fire = null;
		
		try
		{
			fire = new FileReader(FILENAME);
			buff = new BufferedReader(fire);

			String sCurrentLine;

			while ((sCurrentLine = buff.readLine()) != null)
			{
				System.out.println(sCurrentLine);
			}
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		finally {
			
			try {
					if ( buff != null)
						buff.close();
				
					if ( fire != null)
						fire.close();
				} 
			catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
		
	void leituraFornecedores() throws FileNotFoundException
	{
final String FILENAME = "C:\\Users\\matth\\Desktop\\POO TP FINAL\\tp.git\\Trabalho Final\\dados\\fornecedores.dat";
		
		BufferedReader buff = null;
		FileReader fire = null;
		
		try
		{
			fire = new FileReader(FILENAME);
			buff = new BufferedReader(fire);

			String sCurrentLine;

			while ((sCurrentLine = buff.readLine()) != null)
			{
				System.out.println(sCurrentLine);
			}
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		finally {
			
			try {
					if ( buff != null)
						buff.close();
				
					if ( fire != null)
						fire.close();
				} 
			catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	void leituraLicitacoes() throws FileNotFoundException
	{
final String FILENAME = "C:\\Users\\matth\\Desktop\\POO TP FINAL\\tp.git\\Trabalho Final\\dados\\licitacoes.dat";
		
		BufferedReader buff = null;
		FileReader fire = null;
		
		try
		{
			fire = new FileReader(FILENAME);
			buff = new BufferedReader(fire);

			String sCurrentLine;

			while ((sCurrentLine = buff.readLine()) != null)
			{
				System.out.println(sCurrentLine);
			}
		}
		catch (IOException e) 
		{
			e.printStackTrace();
		}
		
		finally {
			
			try {
					if ( buff != null)
						buff.close();
				
					if ( fire != null)
						fire.close();
				} 
			catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}
	
}
