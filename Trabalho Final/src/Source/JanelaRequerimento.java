package Source;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;


public class JanelaRequerimento extends Janela{
	private static final long serialVersionUID = 1L;
	
	static int width = 400;
	static int height = 420;
	private String endereco;
	
	private JButton bRequerimento;
	private RequerimentoHandler hRequerimento;
	
	private JPanel pMain, pRequerimento;
	
	private Container pane = this.getContentPane();
	
	ArrayList<String[]> list = Janela.listBuilder
			(new String[]{  "Nome",    "Produtos"}, 
		     new String[]{ "string", "string"});
	
	private JLabel lTitle, lTitle2;
	
	private JList<String> lCentral;
	
	Central central;
	
	public JanelaRequerimento(Central central){
		super(height, width, "Requerimento");
		
		this.central = central;
		
		pane.setLayout(new BorderLayout());
		
		pRequerimento = Janela.panelBuilder(list);
		Dimension d1 = new Dimension(width, 100);
		pRequerimento.setMinimumSize(d1);
		pRequerimento.setPreferredSize(d1);
		pRequerimento.setMaximumSize(d1);
		pRequerimento.setBorder(new CompoundBorder(new LineBorder(Color.WHITE, 1), new EmptyBorder(10, 10 , 10 ,10)));
		
		bRequerimento = new JButton("Continuar");
		hRequerimento = new RequerimentoHandler();
		bRequerimento.addActionListener(hRequerimento);
		Dimension d2 = new Dimension(width/2, 40);
		bRequerimento.setMinimumSize  (d2);
		bRequerimento.setPreferredSize(d2);
		bRequerimento.setMaximumSize  (d2);
		
		lTitle = new JLabel("Requerimento: ");
		Janela.setColor(lTitle);
		lTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		lTitle2 = new JLabel("Escolha as Centrais para as quais enviar o requerimento");
		Janela.setColor(lTitle2);
		lTitle2.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		DefaultListModel<String> listModel = new DefaultListModel<String>();
		for(Central c : CentralList.read("./dados/centrais.dat").getList()) {
			listModel.addElement(c.getNome());
		}
		lCentral = new JList<>(listModel);
		d2 = new Dimension(width/2, 140);
		lCentral.setMinimumSize  (d2);
		lCentral.setPreferredSize(d2);
		lCentral.setMaximumSize  (d2);
		lCentral.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		
		pMain = new JPanel();
		pMain.setLayout(new BoxLayout(pMain, BoxLayout.Y_AXIS));
		pMain.setBackground(Color.DARK_GRAY);
		pMain.setBorder(new EmptyBorder(10, 10 , 10 ,10));
	
		pMain.add(lTitle);
		pMain.add(Box.createVerticalStrut(10));
		pMain.add(pRequerimento);
		pMain.add(Box.createVerticalStrut(10));
		pMain.add(lTitle2);
		pMain.add(Box.createVerticalStrut(10));
		pMain.add(lCentral);
		pMain.add(Box.createVerticalGlue());
		pMain.add(bRequerimento);
		
		pane.add(pMain);
		this.setVisible(true);
	}
	
	public String getRequerimento(){
		return ((JTextField) pRequerimento.getComponent(1)).getText();
	}
	
	public class RequerimentoHandler implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {
//			ArrayList<Central> l = CentralList.read("./dados/centrais.dat").getList();
//			int i = lCentral.getSelectedIndex();
//			System.out.println("antes");
//			for(Requerimento c2 : l.get(i).getRequerimentos()) {
//				if(c2.getCentral() != null) {
//					System.out.println(c2.getCentral().getNome());
//				}
//			}
//			Central c = l.get(i);
//			c.addRequerimento(new Requerimento(central));
//			l.add(c);
//			System.out.println("depois");
//			for(Requerimento c1 : l.get(i).getRequerimentos()) {
//				if(c1.getCentral() != null) {
//					System.out.println(c1.getCentral().getNome());
//				}
//			}
//			
//			try {
//				File f1 = new File("./dados/centrais.dat");
//				f1.delete();
//				FileOutputStream arquivo1 = new FileOutputStream("./dados/centrais.dat");
//				ObjectOutputStream o = new ObjectOutputStream(arquivo1);
//				o.writeObject(l);
//				o.close();
//			} catch (FileNotFoundException e) {
//				e.printStackTrace();
//			} catch (IOException e) {
//				e.printStackTrace();
//			}
			new JanelaMain();
			frame.dispose();
		}
	}
}
