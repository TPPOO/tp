package Source;

import java.io.Serializable;
import java.util.ArrayList;

public class Central implements Serializable{
	private static final long serialVersionUID = 1L;
	private String nomeEmpresa, nomeCentral;
	private ArrayList<Produto> estoque = new ArrayList<>();
	private ArrayList<Requerimento> requerimentos = new ArrayList<>();

	Central(String nomeEmpresa, String nomeCentral){
		this.nomeEmpresa = nomeEmpresa;
		this.nomeCentral = nomeCentral;
	}

	public String getEmpresa(){
		return nomeEmpresa;
	}
	public String getNome(){
		return nomeCentral;
	}
	public void add(Produto produto){
		estoque.add(produto);
	}
	public void addRequerimento(Requerimento r){
		requerimentos.add(r);
	}
	public ArrayList<Produto> getEstoque(){
		return estoque;
	}
	public ArrayList<Requerimento> getRequerimentos(){
		return requerimentos;
	}
}
