package Source;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class LicitacaoList implements Serializable{
	private static final long serialVersionUID = 1L;
	private ArrayList<Licitacao> list = new ArrayList<>();
	
	public ArrayList<Licitacao> getList() {
		return list;
	}

	public void add(Licitacao l) {
		this.list.add(l);
	}
	
	static LicitacaoList read(String endereco){
		LicitacaoList l = null;
		ObjectInputStream objectIn;
		try {
			objectIn = new ObjectInputStream(new BufferedInputStream(new FileInputStream(endereco)));
			l = (LicitacaoList)objectIn.readObject();
			objectIn.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return l;
	}
}
