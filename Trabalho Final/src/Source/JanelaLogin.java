package Source;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;


public class JanelaLogin extends Janela{
	private static final long serialVersionUID = 1L;
	
	static int width = 300;
	static int height = 200;
	private String endereco;
	
	private JButton bLogin;
	private LoginHandler hLogin;
	
	private JPanel pMain;
	private JPanel pLogin;
	
	private Container pane = this.getContentPane();
	
	ArrayList<String[]> list = Janela.listBuilder
			(new String[]{  "Login",    "Senha"}, 
		     new String[]{ "string", "password"});
	
	public JanelaLogin(String endereco){
		super(height, width, "Login");
		this.endereco = endereco;
		
		pane.setLayout(new BorderLayout());
		
		pLogin = Janela.panelBuilder(list);
		Dimension d1 = new Dimension(width, height/2 - 5);
		pLogin.setMinimumSize(d1);
		pLogin.setPreferredSize(d1);
		pLogin.setMaximumSize(d1);
		pLogin.setBorder(new CompoundBorder(new LineBorder(Color.WHITE, 1), new EmptyBorder(10, 10 , 10 ,10)));
		
		bLogin = new JButton("Login");
		hLogin = new LoginHandler();
		bLogin.addActionListener(hLogin);
		Dimension d2 = new Dimension(width/2, height/6);
		bLogin.setMinimumSize  (d2);
		bLogin.setPreferredSize(d2);
		bLogin.setMaximumSize  (d2);
		
		pMain = new JPanel();
		pMain.setLayout(new BoxLayout(pMain, BoxLayout.Y_AXIS));
		pMain.setBackground(Color.DARK_GRAY);
		pMain.setBorder(new EmptyBorder(10, 10 , 10 ,10));
		pMain.add(pLogin);
		pMain.add(Box.createVerticalStrut(10));
		pMain.add(bLogin);
		
		pane.add(pMain);
		this.setVisible(true);
	}
	
	public String getLogin(){
		return ((JTextField) pLogin.getComponent(1)).getText();
	}
	
	public class LoginHandler implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {
			String login = ((JTextField)pLogin.getComponent(1)).getText();
			if(endereco.intern() == "./dados/centrais.dat") {
				if (login.intern() == "") {
					JOptionPane.showMessageDialog(frame, "Erro 00 - Digite seu login", "Erro", JOptionPane.INFORMATION_MESSAGE);
				}else{
					ObjectInputStream lCentrais;
					CentralList c = null;
					try {
						lCentrais = new ObjectInputStream(new BufferedInputStream(new FileInputStream(endereco)));
						c = (CentralList)lCentrais.readObject();
						lCentrais.close();
					} catch (FileNotFoundException e) {
						JOptionPane.showMessageDialog(frame, "Erro 01 - Arquivo n�o encontrado " + endereco, "Erro", JOptionPane.INFORMATION_MESSAGE);
						e.printStackTrace();
						return;
					} catch (IOException e) {
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
					for(Central i : c.getList()){
						System.out.println(i.getNome());
						if (login.intern() == i.getNome().intern()) {
							new JanelaCentral(i).setVisible(true);
							frame.dispose();
							return;
						}
					}
					JOptionPane.showMessageDialog(frame, "Erro 02 - login n�o encontrado", "Erro", JOptionPane.INFORMATION_MESSAGE);
				}
			}else {
				if (login.intern() == "") {
					JOptionPane.showMessageDialog(frame, "Erro 00 - Digite seu login", "Erro", JOptionPane.INFORMATION_MESSAGE);
				}else{
					ObjectInputStream lFornecedores;
					FornecedorList f = null;
					try {
						lFornecedores = new ObjectInputStream(new BufferedInputStream(new FileInputStream(endereco)));
						f = (FornecedorList)lFornecedores.readObject();
						lFornecedores.close();
					} catch (FileNotFoundException e) {
						JOptionPane.showMessageDialog(frame, "Erro 01 - Arquivo n�o encontrado " + endereco, "Erro", JOptionPane.INFORMATION_MESSAGE);
						e.printStackTrace();
						return;
					} catch (IOException e) {
						e.printStackTrace();
					} catch (ClassNotFoundException e) {
						e.printStackTrace();
					}
					for(Fornecedor i : f.getList()){
						System.out.println(i.getNome());
						if (login.intern() == i.getNome().intern()) {
							new JanelaFornecedor(i).setVisible(true);
							frame.dispose();
							return;
						}
					}
					JOptionPane.showMessageDialog(frame, "Erro 02 - login n�o encontrado", "Erro", JOptionPane.INFORMATION_MESSAGE);
				}
			}
		}
	}
}
