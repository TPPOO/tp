package Source;

import java.io.Serializable;

public class Produto implements Serializable{
	private static final long serialVersionUID = 1L;
	private String nome;
	private int valor;
	
	Produto(String nome, int valor) {
		this.nome = nome;
		this.valor = valor;
	}
	
	public String getNome(){
		return this.nome;
	}
	
	public int getValor(){
		return this.valor;
	}
	
}
