package Source;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class Main {
	public static void main(String[] args) throws FileNotFoundException {
		
		
		
		Central a = new Central("Empresa1", "A");
		Central b = new Central("Empresa1", "ok");
		Central c = new Central("Empresa1", "C");
		
		a.addRequerimento(new Requerimento(b));
		a.addRequerimento(new Requerimento(c));
		b.addRequerimento(new Requerimento(a));
		c.addRequerimento(new Requerimento(a));

		
		CentralList c1 = new CentralList();
		c1.add(a);
		c1.add(b);
		c1.add(c);
		
		try {
			File f1 = new File("./dados/centrais.dat");
			f1.delete();
			FileOutputStream arquivo1 = new FileOutputStream("./dados/centrais.dat");
			ObjectOutputStream o = new ObjectOutputStream(arquivo1);
			o.writeObject(c1);
			o.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		FornecedorList f = new FornecedorList();
		f.add(new Fornecedor("Fornecedor A"));
		f.add(new Fornecedor("Fornecedor B"));
		f.add(new Fornecedor("Fornecedor C"));
		try {
			File f2 = new File("./dados/fornecedores.dat");
			f2.delete();
			FileOutputStream arquivo2 = new FileOutputStream("./dados/fornecedores.dat");
			ObjectOutputStream o = new ObjectOutputStream(arquivo2);
			o.writeObject(f);
			o.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		new JanelaMain();

	}
}


