package Source;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class CentralList implements Serializable{
	private static final long serialVersionUID = 1L;
	private ArrayList<Central> list = new ArrayList<>();
	
	public ArrayList<Central> getList() {
		return list;
	}

	public void add(Central c) {
		this.list.add(c);
	}
	
	static CentralList read(String endereco){
		CentralList l = null;
		ObjectInputStream objectIn;
		try {
			objectIn = new ObjectInputStream(new BufferedInputStream(new FileInputStream(endereco)));
			l = (CentralList)objectIn.readObject();
			objectIn.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
			return null;
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		return l;
	}
}
