package Source;

import java.io.Serializable;
import java.util.ArrayList;

public class Requerimento implements Serializable{
	private static final long serialVersionUID = 1L;
	private Central central;
	private ArrayList<Produto> produtos;

	Requerimento(Central central){
		this.central = central;
	}
	
	public Central getCentral() {
		return central;
	}
}
