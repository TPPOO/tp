package Source;

import java.io.Serializable;
import java.util.ArrayList;

public class Fornecedor implements Serializable{
	private static final long serialVersionUID = 1L;
	private String nome;
	private ArrayList<Produto> Estoque = new ArrayList<>();

	Fornecedor(String nome){
		this.nome = nome;
	}
	
	public void add(Produto produto){
		Estoque.add(produto);
	}
	public ArrayList<Produto> getEstoque(){
		return Estoque;
	}

	public String getNome(){
		return nome;
	}
}
