package Source;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.text.ParseException;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.text.MaskFormatter;

import Source.JanelaLogin.LoginHandler;


public abstract class Janela extends JFrame{
	private static final long serialVersionUID = 1L;
	
	public int margin = 20, size = 30, height = 500, width = 800;
	public static Color bcolor = Color.DARK_GRAY;
	public static Color fcolor = Color.WHITE;
	
	public Janela frame = this;
	public SpringLayout layout;
	public Container pane;
	
	public Janela(int height, int width, String title){
		
		try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }
        
		layout = new SpringLayout();
        pane = this.getContentPane();
        pane.setLayout(layout);
        
        
		setTitle(title);
		setSize(width, height);
		setColor(pane);
		centralizar(this);
		
	    setDefaultCloseOperation(DISPOSE_ON_CLOSE);
	}
	
	private static void centralizar(Janela p) {
	    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
	    int x = (int) ((d.getWidth() - p.getWidth()) / 2);
	    int y = (int) ((d.getHeight() - p.getHeight()) / 2);
	    p.setLocation(x, y);
	}
	
	protected static void setColor(Component c) {
		if(!(c.getClass().equals(JTextField.class) || (c.getClass().equals(JFormattedTextField.class)))) {
			c.setBackground(bcolor);
			c.setForeground(fcolor);
		}
	}
	
	public static JPanel panelBuilder(ArrayList<String[]> list) {
		
		JPanel panel = new JPanel();
		panel.setLayout(new GridLayout(list.size(), 2, 5, 5));
		panel.setBackground(Color.DARK_GRAY);
		
		CharSequence m = "#";
		
		for(String[] a : list) {
			
			panel.add(new JLabel(a[0]));
			
			if(a[1].equals("bool"))
				panel.add(new JCheckBox());
			else if(a[1].equals("string"))
				panel.add(new JTextField());
			else if(a[1].contains(m)){
				
				MaskFormatter mask  = null;
				
				try {
					mask = new MaskFormatter(a[1]);
					mask.setPlaceholderCharacter(' ');
					mask.setValueContainsLiteralCharacters(false);
				
				}catch (ParseException e){
					e.printStackTrace();
				}
				panel.add(new JFormattedTextField(mask));
			}
			else
				panel.add(new JLabel(a[1]));
		}
		while(panel.getComponentCount() < list.size() * 2) {
			panel.add(new JPanel());
		}
		for(Component c : panel.getComponents())
			Janela.setColor(c);
		
		return panel;
	}
	
	public static JPanel FornecedorListBuilder(String endereco){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBackground(Color.DARK_GRAY);
	
		LicitacaoList l = LicitacaoList.read(endereco);
		
		for(Licitacao i : l.getList()){
			panel.add(new panelItem(i));
		}
		
		for(Component c : panel.getComponents())
			Janela.setColor(c);
		
		return panel;
	}
	
	public static JPanel RequerimentoListBuilder(Central central){
		JPanel panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		panel.setBackground(Color.DARK_GRAY);
	
	
		for(Requerimento i : central.getRequerimentos()){
			panel.add(new panelItem(i));
		}
		
		for(Component c : panel.getComponents())
			Janela.setColor(c);
		
		return panel;
	}
	

	public static ArrayList<String[]> listBuilder(String[] l, String[] t) {
		ArrayList<String[]> list = new ArrayList<String[]>();
		
		for(int i = 0; i < l.length; i++) {
			
			list.add(new String[]{l[i], t[i]});
			
		}
		return list;
	}
	
	public static void panelChanger(JPanel p, ArrayList<String[]> list) {
		JPanel pAux;
		p.removeAll();
		pAux = panelBuilder(list);
		p.add(pAux);
	}
	
	public static void panelChanger(JPanel p) {
		JLabel lPadrao = new JLabel("Escolha um tipo de funcionario");
		lPadrao.setHorizontalAlignment(SwingConstants.CENTER);
		setColor(lPadrao);
		
		p.removeAll();
		p.add(lPadrao);
	}
}
