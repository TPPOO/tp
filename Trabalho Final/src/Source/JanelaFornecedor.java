package Source;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.border.*;

import Source.JanelaMain.CentralHandler;
import Source.JanelaMain.FornecedorHandler;

public class JanelaFornecedor extends Janela{
	private static final long serialVersionUID = 1L;

	static int width = 800;
	static int height = 600;
	
	private Fornecedor fornecedor;
	
	private JButton bFornecedor;
	private FornecedorHandler hFornecedor;
	
	private JButton bCentral;
	private CentralHandler hCentral;
	
	private JPanel pLicitacao = new JPanel();
	
	private JLabel lTitle;
	
	private JButton bRefresh;
	private RefreshHandler hRefresh;
	
	
	JanelaFornecedor(Fornecedor fornecedor){
		super(height, width, "Fornecedor");
		
		this.fornecedor = fornecedor;
		
		((JComponent) pane).setBorder(new EmptyBorder(10, 10 , 10 ,10));
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
		pLicitacao = Janela.FornecedorListBuilder("./dados/licitacoes.dat");
		pLicitacao.setAlignmentX(Component.LEFT_ALIGNMENT);
		Dimension d = new Dimension(width/2 - 20, height);
		pLicitacao.setMinimumSize(d);
		pLicitacao.setPreferredSize(d);
		pLicitacao.setMaximumSize(d);
		
		bRefresh = new JButton("Refresh");
		hRefresh = new RefreshHandler();
		bRefresh.addActionListener(hRefresh);
		bRefresh.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		JScrollPane sLicitacao = new JScrollPane(pLicitacao);
		d = new Dimension(width - 40, height - 120);
		sLicitacao.setMinimumSize(d);
		sLicitacao.setPreferredSize(d);
		sLicitacao.setMaximumSize(d);
		sLicitacao.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		lTitle = new JLabel("Licitações disponíveis para " + fornecedor.getNome() );
		Janela.setColor(lTitle);
		lTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		pane.add(lTitle);
		pane.add(Box.createVerticalStrut(10));
		pane.add(sLicitacao);
		pane.add(Box.createVerticalStrut(10));
		pane.add(bRefresh);
		
	}
	public class RefreshHandler implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {
		}
	}
	public void setFornecedor(Fornecedor f) {
		this.fornecedor = f;
	}
}
