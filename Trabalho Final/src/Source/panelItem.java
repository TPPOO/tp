package Source;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;


public class panelItem extends JPanel{
	private static final long serialVersionUID = 1L;

	private JPanel panel = this;
	
	private JButton bNovo, bRejeitar, bProdutos;
	private NovoHandler hNovo;
	private RejeitarHandler hRejeitar;
	private ProdutosHandler hProdutos;
	
	private JLabel lNome, lCentral;
	
	JPanel pane = new JPanel();
	
	private Licitacao l;
	private Requerimento r;
	
	panelItem(Licitacao l){
		this.l = l;
		panel.setBackground(Color.DARK_GRAY);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		panel.setBorder(new EmptyBorder(10, 10 , 0 ,10));
		
		lNome = new JLabel("Empresa: " + l.getCentral().getEmpresa());
		Janela.setColor(lNome);
		
		lCentral = new JLabel("Central: " + l.getCentral().getNome());
		Janela.setColor(lCentral);
		
		bNovo = new JButton("Entrar");
		hNovo = new NovoHandler();
		bNovo.addActionListener(hNovo);
		
		bProdutos = new JButton("Produtos Requeridos");
		hProdutos = new ProdutosHandler();
		bProdutos.addActionListener(hProdutos);
		
		bRejeitar = new JButton("Rejeitar");
		hRejeitar = new RejeitarHandler();
		bRejeitar.addActionListener(hRejeitar);
		
		panel.add(lNome);
		panel.add(Box.createRigidArea(new Dimension(20 , 1)));
		panel.add(lCentral);
		panel.add(Box.createHorizontalGlue());
		panel.add(bProdutos);
		panel.add(Box.createRigidArea(new Dimension(10 , 1)));
		panel.add(bRejeitar);
		panel.add(Box.createRigidArea(new Dimension(10 , 1)));
		panel.add(bNovo);
	}
	
	public panelItem(Requerimento r) {
		this.r = r;
		panel.setBackground(Color.DARK_GRAY);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));
		panel.setBorder(new EmptyBorder(10, 10 , 0 ,10));
		
		lNome = new JLabel("Empresa: " + r.getCentral().getEmpresa());
		Janela.setColor(lNome);
		
		lCentral = new JLabel("Central: " + r.getCentral().getNome());
		Janela.setColor(lCentral);
		
		bNovo = new JButton("Entrar");
		hNovo = new NovoHandler();
		bNovo.addActionListener(hNovo);
		
		bProdutos = new JButton("Produtos Requeridos");
		hProdutos = new ProdutosHandler();
		bProdutos.addActionListener(hProdutos);
		
		bRejeitar = new JButton("Rejeitar");
		hRejeitar = new RejeitarHandler();
		bRejeitar.addActionListener(hRejeitar);
		
		panel.add(lNome);
		panel.add(Box.createRigidArea(new Dimension(20 , 1)));
		panel.add(lCentral);
		panel.add(Box.createHorizontalGlue());
		panel.add(bProdutos);
		panel.add(Box.createRigidArea(new Dimension(10 , 1)));
		panel.add(bRejeitar);
		panel.add(Box.createRigidArea(new Dimension(10 , 1)));
		panel.add(bNovo);
	}

	public class NovoHandler implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {
			new JanelaMain();
			panel.setVisible(false);
		}
	}
	
	public class RejeitarHandler implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {
			if(JOptionPane.showConfirmDialog(pane, "Confirme a exclusao", "alert", JOptionPane.OK_CANCEL_OPTION) == JOptionPane.OK_OPTION) {
				panel.setVisible(false);
			}
		}
	}
	
	public class ProdutosHandler implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {
			new JanelaMain();
		}
	}
}
