package Source;

import java.io.Serializable;
import java.util.ArrayList;

public class FornecedorList implements Serializable{
	private static final long serialVersionUID = 1L;
	private ArrayList<Fornecedor> list = new ArrayList<>();
	
	public ArrayList<Fornecedor> getList() {
		return list;
	}

	public void add(Fornecedor f) {
		this.list.add(f);
	}
}
