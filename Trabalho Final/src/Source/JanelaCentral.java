package Source;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;

import Source.JanelaMain.CentralHandler;

public class JanelaCentral extends Janela{
	private static final long serialVersionUID = 1L;

	static int width = 800;
	static int height = 600;
	
	private Central central;
	
	private JButton bRequerimento;
	private RequerimentoHandler hRequerimento;
	
	private JButton bCentral;
	private CentralHandler hCentral;
	
	private JLabel lTitle;
	private JPanel pMain, pCentrais;
	
	JanelaCentral(Central central){
		super(height, width, "Central" + central.getNome());
		
		((JComponent) pane).setBorder(new EmptyBorder(10, 10 , 10 ,10));
		pane.setLayout(new BoxLayout(pane, BoxLayout.Y_AXIS));
		pCentrais = Janela.RequerimentoListBuilder(central);
		pCentrais.setAlignmentX(Component.LEFT_ALIGNMENT);
		Dimension d = new Dimension(width/2 - 20, height);
		pCentrais.setMinimumSize(d);
		pCentrais.setPreferredSize(d);
		pCentrais.setMaximumSize(d);
		
		bRequerimento = new JButton("Abrir Requerimento de Transferência");
		hRequerimento = new RequerimentoHandler();
		bRequerimento.addActionListener(hRequerimento);
		bRequerimento.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		JScrollPane sCentrais = new JScrollPane(pCentrais);
		d = new Dimension(width - 40, height - 120);
		sCentrais.setMinimumSize(d);
		sCentrais.setPreferredSize(d);
		sCentrais.setMaximumSize(d);
		sCentrais.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		lTitle = new JLabel("Pedidos de transferencia de produtos para a central " + central.getNome());
		Janela.setColor(lTitle);
		lTitle.setAlignmentX(Component.CENTER_ALIGNMENT);
		
		pane.add(lTitle);
		pane.add(Box.createVerticalStrut(10));
		pane.add(sCentrais);
		pane.add(Box.createVerticalStrut(10));
		pane.add(bRequerimento);
		
	}
	public class RefreshHandler implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {
		}
	}
	
	public class RequerimentoHandler implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {
			new JanelaRequerimento(central);
			//frame.dispose();
		}
	}
}
