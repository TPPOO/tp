package Source;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

public class JanelaMain extends Janela{
	private static final long serialVersionUID = 1L;

	private JButton bFornecedor;
	private FornecedorHandler hFornecedor;
	
	private JButton bCentral;
	private CentralHandler hCentral;
	
	private JPanel pMain;
	
	private Janela j;
	
	JanelaMain(){
		super(300, 300, "Gerenciador de Estoque");

		
		
		Container pane = this.getContentPane();
		pane.setLayout(new BorderLayout());
		
		bFornecedor = new JButton("Entrar como Fornecedor");
		hFornecedor = new FornecedorHandler();
		bFornecedor.addActionListener(hFornecedor);
		
		bCentral = new JButton("Entrar como Central");
		hCentral = new CentralHandler();
		bCentral.addActionListener(hCentral);
		
		
		pMain = new JPanel();
		pMain.setLayout(new GridLayout(2, 1, 20, 20));
		pMain.setBackground(Color.DARK_GRAY);
		pMain.setBorder(new EmptyBorder(20, 20 , 20 ,20));
		pMain.add(bFornecedor);
		pMain.add(bCentral);
		
		pane.add(pMain);
		this.setVisible(true);
	}
	
	public class FornecedorHandler implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {
			new JanelaLogin("./dados/fornecedores.dat");
			frame.dispose();
		}
	}
	
	public class CentralHandler implements ActionListener{
		
		public void actionPerformed(ActionEvent arg0) {
			new JanelaLogin("./dados/centrais.dat");
			frame.dispose();
		}
	}
}
